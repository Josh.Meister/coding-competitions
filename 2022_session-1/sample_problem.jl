function solve(N::Int, M::Int, C::Vector{Int})
    candy_amount = sum(C)
    return candy_amount % M
end

T = parse(Int, readline())

for i in 1:T
    N, M = parse.(Int, split(readline()))
    C = parse.(Int, split(readline()))
    y = solve(N, M, C)
    println("Case #", i, ": ", y)
end

