function get_optimal_solution(N::Int, P::Int, preferences::BitMatrix)::Tuple{BitVector, Vector{Int}}
    N_half = fld(N, 2)
    positive_preferences = sum(preferences, dims=1)
    calc_solution(cpref) = cpref > N_half ? true : false
    optimal_solution = calc_solution.(positive_preferences)
    return optimal_solution, positive_preferences
end

function get_compliant_solution(N::Int, M::Int, P::Int, preferences, BitMatrix, ordered_forbids::BitMatrix, optimal_solution::BitVector, positive_preferences::Vector{Int})
end

function order_matrix!(forbids::BitMatrix)
    keys = sum(forbids, dims=)
function solve(N::Int, M::Int, P::Int, preferences::BitMatrix, forbids::BitMatrix)
    # get optimal solution if not for forbidden combinations
    optimal_solution, positive_preferences = get_optimal_solution(N, P, preferences)

    # 

end

T = parse(Int, readline())

for i in 1:T
    N, M, P = parse.(Int, split(readline()))
    preferences = BitArray(undef, (N, P))
    for i = 1:N
        preferences[i, :] = parse.(Bool, split(readline()))
    end
    forbids = BitArray(undef, (M, P))
    for i = 1:M
        forbids[i, :] = parse.(Bool, split(readline()))

    Y = solve(N, M, P, preferences, forbids)
    println("Case #", i, ": ", join(H, " "))
end
