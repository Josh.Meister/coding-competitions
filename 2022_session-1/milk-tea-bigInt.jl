function get_optimal_solution(N::Int, P::Int, preferences::Vector{Int128})::Tuple{Int128, Vector{Int}}
    # this is borked, returns the wrong stuff
    # half converted from previous attempt, need to look at positive_preferences and calc_solution
    N_half = fld(N, 2)
    positive_preferences = get_complaints(P, Int128(0), preferences)
    calc_solution(cpref) = cpref > N_half ? '1' : '0'
    optimal_solution = parse(Int128, join(calc_solution.(positive_preferences)), base=2)
    return optimal_solution, positive_preferences
end

function get_complaints(P::Int, solution::Int128, preferences::Vector{Int128})
    # probably works
    bprefs = string.(preferences, base=2, pad=P)
    counts = zeros(Int, P)
    for bnumber in bprefs
        for (i, b) in enumerate(bnumber)
            if b=='1'
                counts[i] += 1
            end
        end
    end
    return counts
end
    
function get_compliant_solution(N::Int, M::Int, P::Int, preferences::Vector{Int128}, sorted_forbids::Vector{Int128}, optimal_solution::Int128, positive_preferences::Vector{Int})
    # idea not really implemented
    # should look at which combination of bit flips is most optimal
    dissapoints = N .- positive_preferences
    sorted_dissapoints_perm = sortperm(dissapoints)
    sorted_dissapoints = dissapoints[sorted_dissapoints_perm]

    solution = optimal_solution

    function flip_bit!(s::String, pos::Int)
        if s[pos] == '1'
            s[pos] = '0'
            return s
        end
        s[pos] = '1'
    end

    while insorted(solution, sorted_forbids)
        bsolution = string(optimal_solution, base=2, pad=P)
        flip_bit!(bsolution, popfirst!(sorted_dissapoints_perm))
        solution = parse(Int, bsolution, base=2)
    end
    return solution
end

function solve(N::Int, M::Int, P::Int, preferences::Vector{Int128}, forbids::Vector{Int128})
    # get optimal solution if not for forbidden combinations
    optimal_solution, positive_preferences = get_optimal_solution(N, P, preferences)

    # order forbids for better search (hopefully log(n))
    sorted_forbids = sort(forbids)
    solution = get_compliant_solution(N, M, P, preferences, sorted_forbids, optimal_solution, positive_preferences)

    return sum(get_complaints(P, solution, preferences))
end

T = parse(Int, readline())

for i in 1:T
    N, M, P = parse.(Int, split(readline()))
    preferences = Int128[]
    for i = 1:N
        push!(preferences, parse(Int128, readline(), base=2))
    end
    forbids = Int128[]
    for i = 1:M
        push!(forbids, parse(Int128, readline(), base=2))
    end

    Y = solve(N, M, P, preferences, forbids)
    println("Case #", i, ": ", Y)
end
