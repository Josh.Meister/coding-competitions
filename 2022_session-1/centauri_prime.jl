vowels = ['A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u'] 
function solve(kingdom::String)
    end_char = kingdom[end]
    if end_char ∈ vowels
        return "Alice"
    elseif end_char ∈ ['Y', 'y']
        return "nobody"
    end
    return "Bob"
end

T = parse(Int, readline())

for i in 1:T
    kingdom = readline()
    Y = solve(kingdom)
    println("Case #", i, ": ", kingdom, " is ruled by ", Y, ".")
end
