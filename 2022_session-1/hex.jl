function count_stones(N::Int, game::Matrix{Char})
    blue = 0
    red = 0
    empty = 0
    for tile in game[:]
        if tile == 'B'
            blue += 1
        elseif tile == 'R'
            red += 1
        else
            empty += 1
        end
    end
    return (blue, red, empty)
end

function adjacent_squares(N::Int, i::Int, j::Int)
    squares = Tuple{Int, Int}[]
    if j>1
        push!(squares, (i, j-1))
    end
    if j<N
        push!(squares, (i, j+1))
    end
    # do above
    if i>1
        push!(squares, (i-1, j))
        if j < N
            push!(squares, (i-1, j+1))
        end
    end
    # do below
    if i<N
        push!(squares, (i+1, j))
        if j > 1
            push!(squares, (i+1, j-1))
        end
    end
    return squares
end

function print_game(game::Matrix{Char})
    offset = 0
    N = size(game)[1]
    for i in 1:N
        print(" "^offset)
        print(join(game[i, :], " "))
        print("\n")
        offset += 1
    end
end

function top_down_connected(N::Int, game::Matrix{Char}, relevant_char::Char, run_fully::Bool=true, ignore::Tuple{Int, Int}=(-1, -1))
    is_connected = false
    stuff = copy(game)

    if ignore != (-1, -1)
        fieldx, fieldy = ignore
        stuff[fieldx, fieldy] = '.'
    end

    for i in 1:N
        # only look at relevant starts
        if stuff[1, i] != relevant_char
            continue
        end


        if N == 1
            is_connected = true
            if !run_fully
                return is_connected, stuff
            end
        end

        interesting_stuff = [(1, i)]

        while length(interesting_stuff) > 0
            fieldx, fieldy = pop!(interesting_stuff)


            stuff[fieldx, fieldy] = 'X'

            for (new_i, new_j) in adjacent_squares(N, fieldx, fieldy)
                if stuff[new_i, new_j] == relevant_char
                    push!(interesting_stuff, (new_i, new_j))

                    if new_i == N
                        is_connected = true
                        if !run_fully
                            return is_connected, stuff
                        end
                    end
                end
            end
        end
    end
    return is_connected, stuff
end

function last_move_exists!(N::Int, flooded_game::Matrix{Char})
    # replace old X's with new char to avoid confusion or so
    for i in 1:N
        for j in 1:N
            if flooded_game[i, j] == 'X'
                flooded_game[i,j] = 'T'
            end
        end
    end

    for i in 1:N
        for j in 1:N
            if flooded_game[i, j] == 'T'
                still_connected, = top_down_connected(N, flooded_game, 'T', false, (i,j))
                if !still_connected
                    return true
                end
            end
        end
    end
    return false
end

function solve(N::Int, game::Matrix{Char})
    blue, red, empty = count_stones(N, game)

    # check for trivial impossible
    if abs(blue-red)>1
        return "Impossible"
    end

    red_connected, connected_game = top_down_connected(N, game, 'R')
    if red_connected == true
        # blue can't place a stone after the game is over
        if blue - red == 1
            return "Impossible"
        end

        # check if there is a "last move" without red wouldn't win
        if last_move_exists!(N, connected_game)
            return "Red wins"
        else
            return "Impossible"
        end
    end
    blue_connected, connected_game = top_down_connected(N, permutedims(game, (2, 1)), 'B')
    if blue_connected == true
        # red can't place a stone after the game is over
        if red - blue == 1
            return "Impossible"
        end

        # check if there is a "last move" without blue wouldn't win
        if last_move_exists!(N, connected_game)
            return "Blue wins"
        else
            return "Impossible"
        end
    end

    return "Nobody wins"
end

# N = 4
# game = ['B' 'B' 'B' 'B'; 'B' 'B' 'B' '.'; 'R' 'R' 'R' '.'; 'R' 'R' 'R' 'R']
# print_game(game)
# print(solve(N, game))
T = parse(Int, readline())

for i in 1:T
    # parse game
    N = parse(Int, readline())
    game = fill('.', (N, N))
    for i in 1:N
        row = readline()
        for j in 1:N
            game[i, j] = row[j]
        end
    end
    y = solve(N, game)
    println("Case #", i, ": ", y)
end
