function generate_testset(N::Int)
    rng = 1:100000
    ts = zeros(Int, N)
    for i in 1:N
        ts[i] = rand(rng)
    end
    return ts
end

function sorted_push!(vec::Vector{Int}, element::Int)
    for (i, value) in enumerate(vec)
        if element > value
            insert!(vec, i, element)
            return vec
        end
    end  
    push!(vec, element)
end

function update_citation_numbers!(rc::Vector{Int}, Cᵢ::Int)
    rc_length = length(rc)
    if Cᵢ >= rc_length
        sorted_push!(rc, Cᵢ)

        if rc[end] < rc_length+1
            pop!(rc)
        end
    end
    return rc
end

function generate_citation_numbers(N::Int, C::Vector{Int})
    rc = Int[]
    H = zeros(Int, N)

    for (i, Cᵢ) in enumerate(C)
        H[i] = length(update_citation_numbers!(rc, Cᵢ))
    end
    return H
end

@time ts = generate_testset(100000)
@time generate_citation_numbers(100000, ts)

# T = parse(Int, readline())

# for i in 1:T
#     N = parse(Int, readline())
#     C = parse.(Int, split(readline()))
#     H = generate_citation_numbers(N, C)
#     println("Case #", i, ": ", join(H, " "))
# end
