function solve(N::Int, M::Int, C::Vector{Int})::Int
    ΣC = sum(C)
    return ΣC%M
end

T = parse(Int, readline())

for i in 1:T
    N, M = parse.(Int, split(readline()))
    C = parse.(Int, split(readline()))
    y = solve(N, M, C)
    println("Case #$i: $y")
end
