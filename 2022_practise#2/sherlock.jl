function solve(L::Int, R::Int)::Int
    # according to scirbbling for the ()()()()... scheme

    # number of machting pharentesis pairs in the ()() structure
    P = min(L,R)

    # sum of increasing integers, thats what its gotta be!
    Σ = (P+1)*P/2

    return Σ
end

T = parse(Int, readline())

for i in 1:T
    L, R = parse.(Int, split(readline()))
    y = solve(L, R)
    println("Case #$i: $y")
end
