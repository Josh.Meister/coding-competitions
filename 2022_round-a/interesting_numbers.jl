function is_interesting(N::Vector{Char})
    prod = 1
    sum = 0
    for i in N
        n = parse(Int, i)
        prod *= n
        sum += n
    end
    if prod%sum==0
        return true
    end
    return false
end

function jump!(A::Vector{Char}, indx::Int)
    for i in indx:length(A)
        A[i] = '1'
    end
    return A
end

function preprocess_B(B::Int)
    count = 0

    B_str = collect(string(B))

    zero_indx = findfirst(l->l=='0', B_str)
    if zero_indx == nothing || zero_indx == length(B_str)
        return B, 0
    end
    B_str[zero_indx-1] = Char('0'+parse(Int, B_str[zero_indx-1]) - 1)
    for i in zero_indx:length(B_str)
        B_str[i] = '9'
    end
    B_new = parse(Int, join(B_str))
    return B_new, B-B_new
end
function solve(A::Int, B::Int)
    funky_skipped = 0
    candidate = A
    B, count = preprocess_B(B)

    B_length = floor(Int, log10(B)+1)
    candidate_str = collect(string(candidate, pad=B_length))

    # init checksum
    checksum = sum([i for i in candidate_str])

    # init vector of base products
    prodv = zeros(Int, length(candidate_str)-1)
    for i in 1:length(candidate_str)-1
        prod = 1
        for k in 1:i
            prod *= parse(Int, candidate_str[k])
        end
        prodv[i] = prod
    end

    while candidate <= B
        candidate_str = collect(string(candidate))

        # sieve zeros and jump
        zero_indx = findfirst(l->l=='0',candidate_str)
        if zero_indx != nothing
            jump!(candidate_str, zero_indx)
            next_candidate = parse(Int, join(candidate_str))
            count += next_candidate - candidate
            funky_skipped += next_candidate-candidate
            for i in candidate:next_candidate
                printstyled(i, ", ", color=:blue)
            end
            checksum += (length(candidate_str)-zero_indx+1) |> (n -> 1-8*n)
            candidate = next_candidate
        end

        # interesting check
        if is_interesting(candidate_str)
            count += 1
            printstyled(candidate, ", ", color=:green)
        else
            printstyled(candidate, ", ", color=:red)
        end

        candidate += 1
        checksum += 1
    end
    println("Skipped ", round(funky_skipped/(B-A)*100, digits=1), "% due to funky zero thingy")
    println("About ", round(count/(B-A)*100, digits=1), "% of numbers were interesting")
    return count
end

#T = parse(Int, readline())
T = 10

for i in 1:T
    #A,B = parse.(Int, split(readline()))
    A,B = rand(1:Int(10^8), 2)
    A,B = B > A ? (A,B) : (B,A)
    y = solve(A, B)
    println("Case #", i, ": ", y)
end
