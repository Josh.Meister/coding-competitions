function is_bigger(A::Vector{Int}, B::Vector{Int})
    # A and B have same size
    for i in 1:length(A)
        if A[i] > B[i]
            return true
        elseif A[i] == B[i]
            continue
        else
            return false
        end
    end
    return false
end

function funky_best(N::Vector{Int}, diff::Int)
    start = diff==0 ? 2 : 1
    base = insert!(copy(N), start, diff)
    for i in (start+1):length(N)+1
        new_base = insert!(copy(N), i, diff)
        if !is_bigger(new_base, base)
            base = new_base
        end
    end
    return base
end

function insert_best!(N::Vector{Int}, diff::Int)
    start = diff==0 ? 2 : 1
    for i in start:length(N)
        if diff < N[i]
            insert!(N, i, diff)
            return N
        end
    end
    push!(N, diff)
    return N
end
function solve(N::Vector{Int})
    checksum = sum(N)
    diff = 9-checksum%9

    # diff is 9 sometimes, gnarly bug meeeh
    diff = diff%9

    old_N = copy(N)
    insert_best!(N, diff)
    return join(N)
end

T = parse(Int, readline())

for i in 1:T
    N = parse.(Int, collect(readline()))
    y = solve(N)
    println("Case #", i, ": ", y)
end
