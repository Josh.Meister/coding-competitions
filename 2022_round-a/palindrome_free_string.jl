function is_odd_palindrome(sample::Vector{Char}, center_indx::Int, n::Int)
    symmetric_offset = Int((n-1)/2)

    left_view = sample[center_indx-symmetric_offset:center_indx-1]
    right_view = sample[center_indx+symmetric_offset:-1:center_indx+1]

    if left_view == right_view
        return true
    end
    return false
end

function is_even_palindrome(sample::Vector{Char}, left_center_indx::Int, n::Int)
    symmetric_offset = Int(n/2)

    left_view = sample[left_center_indx-symmetric_offset+1:left_center_indx]
    right_view = sample[left_center_indx+symmetric_offset:-1:(left_center_indx+1)]
    if left_view == right_view
        return true
    end
    return false
end

function contains_palindrome(sample::Vector{Char}, minlength::Int, maxlength::Int)
    for i in fld(minlength, 2):length(sample)-fld(minlength, 2)
        check_limit = min(maxlength, 2*i, 2*(length(sample)-i)+1)
        for l in minlength:check_limit
            if iseven(l)
                if is_even_palindrome(sample, i, l)
                    return true
                end
            else
                if is_odd_palindrome(sample, i, l)
                    return true
                end
            end
        end
    end
    return false
end

mutable struct Node
    name::Tuple{Int, Bool}
    parent::Tuple{Int, Bool}
    children::Vector{Tuple{Int, Bool}}
    isroot::Bool=false
end

function solve(S::Vector{Char})
    # search from left to right

    # do depth first search?
    qindex = findnext('?', S)
    if qindex == nothing
        if contains_palindrome(S, 5, 100000)
            return "IMPOSSIBLE"
        else
            return "POSSIBLE"
        end
    end
    nextqindex = findnext('?', S, qindex)
    nextqindex = nextqindex == nothing ? length(S) : nextqindex
    
    
    interesting_nodes = [(qindex)]
    while length(interesting_nodes) > 0
        node = pop!(interesting_nodes)
        next_
    end
    return 1
end

T = parse(Int, readline())

for i in 1:T
    S = collect(readline())
    y = solve(S)
    println("Case #", i, ": ", y)
end
