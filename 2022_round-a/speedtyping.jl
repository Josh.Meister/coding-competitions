function trim!(I::Vector{Char}, P::Vector{Char}, startpos::Int)
    for i in startpos:length(P)
        if i>length(I) || I[i] != P[i]
            P = deleteat!(P, i)
            return 1, P, i
        end
    end
    return 0, P, length(P)
end

function solve(I::String, P::String)
    I = collect(I)
    P = collect(P)

    changes = 0
    endpos = 1

    while I != P
        newchanges, P, endpos = trim!(I, P, endpos)
        if newchanges == 0
            if I != P
                return "IMPOSSIBLE"
            else
                return string(changes)
            end
        end
        changes += newchanges
    end
    return string(changes)
end

T = parse(Int, readline())

for i in 1:T
    I = readline()
    P = readline()

    y = solve(I, P)
    println("Case #", i, ": ", y)
end
