function solve(N::Int, pw::String)
    # check for missing characters
    has_lowercase = false
    has_uppercase = false
    has_digit = false
    has_special_char = false

    special_chars = ['#', '@', '*', '&']

    for c in pw
        if isuppercase(c)
            has_uppercase = true
        elseif islowercase(c)
            has_lowercase = true
        elseif isdigit(c)
            has_digit = true
        elseif c in special_chars
            has_special_char = true
        end
    end

    # append missing characters
    if !has_uppercase
        pw *= 'A'
        N += 1
    end
    if !has_lowercase
        pw *= 'a'
        N += 1
    end
    if !has_digit
        pw *= '1'
        N += 1
    end
    if !has_special_char
        pw *= '#'
        N += 1
    end

    if N<7
        pw *= 'a'^(7-N)
    end
    return pw
end

T = parse(Int, readline())

for i in 1:T
    pwlength = parse(Int, readline())
    pw = readline()
    pw = solve(pwlength, pw)
    println("Case #", i, ": ", pw)
end
