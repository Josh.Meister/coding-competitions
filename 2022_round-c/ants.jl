function init_collision_stack(N::Int, P::Vector{Int}, D::Vector{Int})
    # we need a collision stack from which we pop and insert to know when to simulate to

    # Distance, Position, Indx1
    cstack = Tuple{Int, Rational{Int}, Int}[]
    for i in 1:N-1
        if D[i] == 1 && D[i+1] == -1
            collision_distance = P[i+1]-P[i]
            append!(cstack, (collsision_distance, P[i]+collision_distance//2))
        end
    end
    sort!(cstack, by=x->x[1])
    return cstack
end

# time and distance shall be equivalent in value
# no ant shall reach the end during sim
# no collisions shall take place during sim
function sim_to_time!(N::Int, time::Int, P::Vector{Int}, D::Vector{Int})
    # this should wor
    # k
    P += D*time
    return P
end

# function find_sorted!(a, by)
    # perform binary search on sorted collection a for item matching b
# end
function insert_in_sorted!(cstack::Vector{Tuple{Int, Rational{Int}, Int}}, collision::Tuple{int, Rational{Int}, Int})
    # should do binary search in cstack distance to keep it sorted...
    # just do binary search for left element smaller, right element larger
   
    if collision[1] <= cstack[1][1]
        pushfirst!(cstack, collision)
    elseif colission[end] >= cstack[end][1]
        push!(cstack, collision)
    end

    # this is linear search. Ideally it would be binary, since cstack is already sorted by 1st index
    for i in 1:length(cstack)
        if collision[1] < cstack[i][1]
            insert!(cstack, i, collision)
            return
        end
    end
    push!(cstack, collision)
end

function solve(N::Int, L::Int, Labels::Vector{Int}, P::Vector{Int}, D::Vector{Int})::Vector{Int}
    # lmao funny argument names
    cstack = init_collision_stack(N, P, D)


end

T = parse(Int, readline())

for i in 1:T
    # read
    N, L = parse.(Int, split(readline()) )

    P = zeros(Int, N)
    D = zeros(Int, N)
    for i in 1:N
        P[i], D[i] = parse.(Int, split(readline()))
    end

    # transform D to speed vector
    for i in 1:length(D)
        if D[i] == 0
            D[i] = -1
        end
    end

    # sort P and D to be the ants position in ascending order
    labels = collect(1:L)
    I = sortperm(P)
    P = P[i]
    D = D[i]

    output = solve(N, L, P, D)

    println("Case #", i, ": ", join(output, ' '))
end
