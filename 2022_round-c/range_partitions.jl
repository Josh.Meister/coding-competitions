function findA(N::Int, ∑N::Int, ∑A::Int)
    A = Int[]
    ∑A_c = 0
    biggest_N = N
    while ∑A - ∑A_c > biggest_N
        push!(A, biggest_N)
        ∑A_c += biggest_N
        biggest_N -= 1
    end
    push!(A, ∑A-∑A_c)
    return A
end

fast_sequential_sum(N::Int)::Int = (N+1)*N/2

function solve(N::Int, X::Int, Y::Int)::Tuple{String, Int, Vector{Int}}
    ∑N = fast_sequential_sum(N)

    d_max = max(fld(∑N, X), fld(∑N, Y))
    for d in 1:d_max
        ∑A = X*d
        ∑B = ∑N - ∑A
        if ∑B == Y*d
            A = findA(N, ∑N, ∑A)
            return "POSSIBLE", length(A), A
        end
    end
    return "IMPOSSIBLE", 0, []
end

T = parse(Int, readline())

for i in 1:T
    N, X, Y = parse.(Int, split(readline()) )
    output, A_len, A = solve(N, X, Y)
    println("Case #", i, ": ", output)
    if output == "POSSIBLE"
        println(A_len)
        println(join(A, ' '))
    end
end
